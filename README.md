### GIT Summary  

1. Summary:  
```    
  ! Git is a distributed version control:  
    + keeps track of changes, not versions  
    + especially text changes  
    + no need to communicate with a central server  

  ? When should use Git?
    - wanting to track edits  
      + review a history log of changes made  
      + view differences between versions  
      + retrieve old versions  
    - need to share changes with collaborators  
    - not afraid of command line tools  
  ? When not use Git?  
    - not as useful for tracking non-text files: images, movies, music, fonts, ...  
```  
2. Installation:  
```  
  **Set up**
  - Git website: https://www.git-scm.com  
  - command line:  
    + which git = return path git stored  
    + git --version = return version of git installed  

  **Configure**  
  There are 3 types of configuration:  
    + System  
      /etc/gitconfig  
      Program FIles\Git\etc\gitconfig  

      => git config --system  
    + User  
      ~/.gitconfig  
      $HOME\.gitconfig  

      => git config --global  
    + Project  
      my_project/.git/config  

      => git config  
    Note:  
      + git config --list = return list config  
      + git config --global color.ui true  
      + git config --global core.editor "mate -wl1"  

  **Git auto-completion**  
    cd ~  
    curl -OL https://github.com/git/git/raw/master/contrib/completion/git-completion.bash  

    mv ~/git-completion.bash ~/.git-completion.bash  

    nano ~/.bashrc or ~/.bash_profile  

    if [ -f ~/.git-completion.bash ]; then
     source ~/.git-completion.bash  
    fi  
  **Git help**  
  - command line:  
    git help <command>  

    f to forward, b for backward  

    man git-<command> => git help <command>    
```  
3. Getting Started  
```  
  **Initializing a repository**  
    git init
  **Understaning where GIt files are stored**
    ls -la .git
  **Commit**  
   - Write commit:  
      git add . = add all changes  
      git commit -m "<message>" = add message for commit

      OR  
      git commit -am "<message>"

      Note:  
        - commit message:
          + short single-line summary (less than 50 characters)  
          + optionally followed by a blank line and a more complete description  
          + keep each line to less than 72 characters  
          + write commit messages in present tense, not past
          + try using shorthand:  
            "[css, js]"  
            "bugfix: "  
            "#38405 - "  
    - Watch commit:  
      git log (--pretty=oneline)  

      git log -n 1 = watch commit by order
      git log --since = watch commit by date  
      git long --until = watch commit until date
```  
4. Git concepts:  
```  
  **Architecture**
    **Two-tree architecture**  
    repository ---- working  

    working ----commit---->repository
    repository--checkout-->working

    **Three-tree architecture**  

    working-----staging index----repository  

    working---git add--->staging index  
    staging index--git commit--->repository  
    repository---checkout----->working  
  **Git workflow**  
    Every change identified with sha-1 key (checksum) and git control these changes through versions.
    - SHA-1:  
      + 40-character hex string (0-9, a-f)  
    Get info of commit through snapshot  
  **HEAD pointer**  
    + pointer to "tip" of current branch in repository  
    + last state of repo, what was last checked out  
    + points to parent of next commit  
      where writing commits takes place    
```  
5. Making changes to files:  
```
  **Adding files**  
    - Step1: git status (optional) = know how many changed files.
    - Step2: git add .  
      + git add . = add all of changed files  
      + git add <file_name1.extension> <file_name2.ext>
    - Step3: git commit -m "message"  
    - Shorthand: git commit -am "message"  

  **Editing files**  
    + using git add to add file to staging directory  
    + using git checkout filename to discard this file.  

  **Viewing changes with diff**  
    + git diff = see whole changes between all files in working directory  
    + git diff filename.ext = see only changes within specific file.  
    + git diff banchname1 branchname2 = see differences between two branches  

  **Viewing only staged changes**  
    + git diff --staged/--cache = watch changes in staging index  

  **Deleting files**  
    + Delete files by hand  
    + Delete files by: git rm filename.ext  

  **Moving and renaming files**  
    - Rename file:  
      git mv filename1.ext filname2.ext  
      git status  
    - Moving and Rename files:  
        git mv filname1.ext name_folder/filename2.ext  
```  
6. Undoing changes:  
```
  **Undoing working directory changes**  
      git chekout -- filename.ext  
    note: -- : avoid checkout new branch  
  **Ustaging files**  
      git reset HEAD filename.ext  
  **Amending commits**  
      git commit -amend -m "message" = changes message for last commit
  **Retrieving old versions**  
      git checkout SHA-1 -- filename.ext  
      git reset HEAD filename.ext  
  **Reverting a commit**  
      git revert SHA-1  
  **Using reset to undo commits**  
      git reset SHA-1 ==> move the HEAD pointer  
    --soft:  
      + does not change staging index or working directory  
    --mixed (default)  
      + changes staging index to match repository  
      + doesn't change working directory  
    --hard  
      - changes staging index and working directory to match repository  
    Note: in fact, files just hidden not throw away, so can retrieve it again, back and next commit.
  **Removing untracked files*  
    git clean -n/-f
      -n : would removing files
      -f : force removing files
```  
7. Ignore files:  
```  
  **Using .gitignore files**
    - file lets git know which files need to be ignored.  
    - negate expressions with !  
      + *.php = ignore all file with php extension.  
      + !index.php = except index.php  
    - very basic regular expressions  
    - ignore all files in a directory with trailing slash  
    - comment lines begin with #, blank lines are skipped  
        nano .gitignore  
  **Understaning what to ignore**  
    + compiled source code  
    + packages and compressed files  
    + logs and databases  
    + operating system generated files  
    + user-uploaded assets (images, PDFs, videos)  
  **Ignoring files globally**  
    + ignore files in all repository  
    + settings not tracked in repository  
    + user-specific instead of repository-specific  

      git config --global core.excludesfile ~/.gitignore_global  
  **Ignoring tracked files**  
    Make git stop tracking file first:  
      git rm --cached filemame.ext  
      (staging index)  
    If not stopping tracking file, git can not ignore file.  
  **Tracking empty directories**
    + Git just keeps track files not track directories.  
    + Make a trick:
      touch direc/.git_keep  
```  
8. Navigating the Commit Tree  
```  
  **Referencing commits**  
  tree-ish: reference to commits  
    - full SHA-1 hash  
    - short SHA-1 hash  
      + at least 4 characters  
      + umambiguous (8-10 characters)  
    - HEAD pointer  
    - branch reference, tag reference  
    - ancestry  
      - parent commit  
        + HEAD^, SHA^, master^ (moving up)
        + HEAD~1, HEAD~ (going back)
      - grandparent commit  
        + HEAD^^, SHA^^, master^^
        + HEAD~2
      - great-grandparent commit  
        + HEAD^^^, SHA^^^, master^^^  
        + HEAD~3  

  => git ls-tree master/HEAD
    blob = file
    tree = directory  
  **Commit log**  
    git log --oneline    = show commit only one line  
    git log --oneline -n = show commit only one line with n commits
    git log --since=date = show commits after date  
    git log --until=date = show commits unti date
    git log --author="name" = show commits only of specific author  
    git log --grep"regx" = show commits with message contain "regx"
    git log -p/--stat --summary  
      -p : path  
      --stat
      --summary
    git log --format=oneline/full/medium/short  
    git log --graph  
    git log --oneline --graph --all --decorate
  **Viewing commits**  
      git show SHA  
  **Comparing commits**  
      git diff SHA
      git diff SHA..SHA
      git diff -w SHA..HEAD  
        -w = --ignore-all-space
```  
9. Branching  
```  
  **Viewing and creating branches**  
    - Viewing  
      git branch    = show which branch you are at  
      git branch -a = show all branches
    - Creating  
      git branch branch_name    = create new branch named branch_name  
  **Switching branches**  
    git checkout branch_name    = switch to branch named branch_name  
  **Creating and Switching branches**  
    git checkout -b branch_name = create and switch to branch named branch_name  
  **Switching branches with uncommitted changes**  
    Need to commit first or stash and apply later  
  **Comparing branches**  
    git diff (--color-words) master...branch_name  
    git branch --merged = This command finds best common ancestor(s) between two commits. And if the common ancestor is identical to the last commit of a "branch" ,then we can safely assume that that a "branch" has been already merged into the master. Run command git merge-base <commit-hash-step1> <commit-hash-step2>   
  **Renaming branches**  
    git branch --move (or short -m) target_branch new_branch = rename branch from target_branch to new_branch  
  **Deleting branches**  
    git branch --delete (or short -d) target_branch = delete target_branch  
    git branch -D target_branch: force delete target_branch  
  **Configuring the command prompt to show the branch**  
    cd ~
    __git_ps1  
    cd ..
    __git_ps1  
    cd git_project  
    echo $PS1

    export PS1='>>>>'
    >>>>>export PS1='-->'
    -->export PS1='ild$ '

    export PS1='\W$(__git_ps1 "(%s)") > '
```  
10. Merging Branches:  
```  
  **Merging code**  
    Step 1: git checkout target_wanted_merge_to_brach
    Step 2: git merge target_branch = merge target branch to current branch  
  **Using fast-forward merge vs true merge  
    - fast-forward: if current branch doesn't has any new commit, urrent HEAD is an ancestor of the commit you're trying to merge, this merge call fast forward, will move the HEAD commit in new branch to current branch
    - true merge: if has any commits in current branch, merge will create new commit into current branch  
      git merge --no-ff target_branch = create new commit, no execute fast-forward
      git merge --ff-only target_branch = execute fast-forward if possible  
  **Merging conflicts**  
    - occur when two branch edit same files.  
  **Resolving merge conflicts**  
      - git merge --abort  
      - fix by hand in conflict files
  **Reduce merge conflicts**  
      - keep lines short  
      - keep commits small and focused  
      - beware stray edits to whitespace  
        + spaces, tabs, line returns  
      - merge often  
      - track changes to master  
```  
11. Stashing Changes:  
```  
  **Saving changes in the stash**  
    - usage: save the changes somewhere and get back when need  
    - command line:
            git stash save "<message>"  
  **Viewing stashed changes**  
    - command line:  
        git stash list = view list of stashes, e.g: stash{n}
        git stash show (-p) stash{n} = view what changes in that stash
  **Retrieving stashed changes**  
    - command line:  
        git stash apply stash{n} = copy it  
        git stash pop stash{n} = pop it out  
  **Deleting stashed changed**  
    - command line:  
        git stash drop stash{n} = drop a specific stash  
        git stash clear = clear all stashes
```
12. Remotes:  
```  
    **Local and remote repositories**  
        computer ---push---> remote server
        computer <---fetch + merge-- remote server  
    **Setting up a GitHub account**
        - create a account on Git, bitbucket, gitlab,...
    **Adding a remote repository**  
        - command line:  
            git remote (-v) = show all remotes  
            git remote add origin <link> = link local repository to remote server
            git remote rm <name_remote> = remove specific remote
    **Creating a remote branch**  
        - command line:  
            git push --set-upstream (-u) origin = track remote
            git branch -r = remote branch  
            git branch -a = all branches  
    **Clone a remote repository**  
        - command line:  
            git clone <path> <name_directory> = clone a remote  
    **Tracking remote branches**  
        - command line:  
            git config branch.<name_branch>.merge refs/heads/master  
            .git/config by hand  
    **Pushing changes to a remote repository**  
        - command line:  
            git push origin master/git push (if tracked)
    **Fetching changes from a remote repository**  
        - command line:  
            git fetch <remote>  = get new changes in remote server  
        Note:  
            fetch before working  
            fetch before push  
            fetch often    
    **Merging in fetched changes**  
        - command line:  
            git merge <remote> = merging remote server to local  
            git pull = git fetch + git merge  
    **Checking out remote branches**
        - can't checkout the branch, need to create new branch and track
        - command line:  
            git branch (-b) <new_branch> <remote_branch>
    **Pushing to an updated remote branch**  
        - command line:  
            git fetch --> git merge --> fix conflict --> git push  
    **Deleting a remote branch**  
        - command line:  
            git push <remote> :<branch> = push local branch to remote branch and delete this remote  
            or  
            git push <remote> --delete <branch>
    **Enabling collaboration**  
        - Setting > Collaborators > Search Account/Email > Add  
        - Fork ---then--- Pull request  
        - Put issues to Issues  
    **A collaboration workflow**  
        - command line:  
            git checkout <branch>  
            git fetch  
            git merge <remote_branch>  
            git checkout -b <new_branch>  
            ...
            git fetch  
            git push -u <remote> <new_branch>  
            git log -p <new_branch>...<remote_branch>

            git checkout <branch>  
            git fetch  
            git merge <remote_branch>  
            git checkout -b <new_branch>  <remote_branch>
            git log  
            git show sha-hash  
            ....
```  
14. Tools and Next Steps:  
```  
    **Setting up aliases for common commands**  
        - command line:  
            git config --global alias.st "status"  
            git config --global alias.co checkout
            git config --global alias.ci commit
            git config --global alias.br branch  
            git config --global alias.df diff  
            git config --global alias.dfs "diff --staged"
            git config --global alias.logg "log --graph --decorate --oneline --abbrev-commit --all"
            (saving config in gitconfig)  
    **Using SSH keys for remote login**  
        - Caching password:  
            git credential-osxkeychain  
        - Generating SSH Keys:  
            Step1:  cd ~/.ssh  
            Step2:  ssh-keygen -t rsa -b 4096 -C "email"
            Step3: copy id_rsa.pub  
    **Exploring integrated development environments**  
        - Vim  
        - Emacs  
        - TextMate  
        - Eclipse  
        - Netbean  
        ...  
    **Exploring graphical user interfaces**  
        - Window: TortoiseGit, Github, SmartGit
    **Understanding Git hosting**  
        - GitHub  
        - Bitbucket  
        - Gitorious  

        Git self-hosting  
        - Gitosis  
        - Gitolite  
        - Gitlab
```  
